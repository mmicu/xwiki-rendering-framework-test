package xwiki.rendering.unit.test.api.block;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.block.Block;
import org.xwiki.rendering.block.TableBlock;
import org.xwiki.rendering.block.WordBlock;

@RunWith(JUnitParamsRunner.class)
public class TestTableBlock
{
    @SuppressWarnings("unused")
    private Object[] wordBlockInstances () 
    {
        return new Object[]{
            new Object[] { new WordBlock ("word_1"), new WordBlock ("word_2"), new WordBlock ("word_3") },
            new Object[] { new WordBlock ("word_4"), new WordBlock ("word_5"), new WordBlock ("word_6") },
            new Object[] { new WordBlock ("word_7"), new WordBlock ("word_8"), new WordBlock ("word_9") }
        };
    }
    
    @Test
    @Parameters(method = "wordBlockInstances")
    public void testTableBlockConstructor (Block w_1, Block w_2, Block w_3)
    {
        TableBlock tb = new TableBlock (Arrays.asList (w_1, w_2, w_3));
        
        assertTrue (tb.getChildren ().size () == 3);
        assertEquals (tb.getChildren ().get (0), w_1);
        assertEquals (tb.getChildren ().get (1), w_2);
        assertEquals (tb.getChildren ().get (2), w_3);
    }
    
    @Test
    @Parameters(method = "wordBlockInstances")
    public void testChildInsertAfter (Block w_1, Block w_2, Block w_3)
    {
        // w_1
        TableBlock tb = new TableBlock (Arrays.asList (w_1));
        
        assertTrue (tb.getChildren ().size () == 1);
        assertEquals (tb.getChildren ().get (0), w_1);
        
        // w_1 --> w_2
        tb.insertChildAfter (w_2, w_1);
        assertTrue (tb.getChildren ().size () == 2);
        assertEquals (tb.getChildren ().get (0), w_1);
        assertEquals (tb.getChildren ().get (1), w_2);
    }
    
    @Test
    @Parameters(method = "wordBlockInstances")
    public void testChildInsertBefore (Block w_1, Block w_2, Block w_3)
    {
        // w_1 --> w_2
        TableBlock tb = new TableBlock (Arrays.asList (w_1, w_2));
        
        assertTrue (tb.getChildren ().size () == 2);
        assertEquals (tb.getChildren ().get (0), w_1);
        assertEquals (tb.getChildren ().get (1), w_2);
        
        // w_1 --> w_3 --> w_2
        tb.insertChildBefore (w_3, w_2);
        assertTrue (tb.getChildren ().size () == 3);
        assertEquals (tb.getChildren ().get (0), w_1);
        assertEquals (tb.getChildren ().get (1), w_3);
        assertEquals (tb.getChildren ().get (2), w_2);
    }
}
