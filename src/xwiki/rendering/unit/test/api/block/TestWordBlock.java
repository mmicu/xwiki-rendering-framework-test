package xwiki.rendering.unit.test.api.block;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.block.WordBlock;

@RunWith(JUnitParamsRunner.class)
public class TestWordBlock
{
    @SuppressWarnings("unused")
    private Object[] wordBlockInstancesEquals () 
    {
        return new Object[]{
            new Object[] { new WordBlock ("word_1"), new WordBlock ("word_1") },
            new Object[] { new WordBlock ("word_2"), new WordBlock ("word_2") },
            new Object[] { new WordBlock ("word_3"), new WordBlock ("word_3") }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] wordBlockInstancesNotEquals () 
    {
        return new Object[]{
            new Object[] { new WordBlock ("word_1"), new WordBlock ("word_2") },
            new Object[] { new WordBlock ("word_2"), new WordBlock ("word_3") },
            new Object[] { new WordBlock ("word_4"), new WordBlock ("word_3") }
        };
    }
    
    @Test
    @Parameters(method = "wordBlockInstancesEquals")
    public void testEqualsWordBlocks (WordBlock w_1, WordBlock w_2)
    {
        assertEquals (w_1, w_2);
    }
    
    @Test
    @Parameters(method = "wordBlockInstancesNotEquals")
    public void testNotEqualsWordBlocks (WordBlock w_1, WordBlock w_2)
    {
        assertFalse (w_1.equals (w_2));
    }
}
