package xwiki.rendering.unit.test.api.internal.block;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.xwiki.rendering.block.ImageBlock;
import org.xwiki.rendering.block.LinkBlock;
import org.xwiki.rendering.block.RawBlock;
import org.xwiki.rendering.block.TableBlock;
import org.xwiki.rendering.block.WordBlock;
import org.xwiki.rendering.block.match.BlockMatcher;
import org.xwiki.rendering.block.match.ClassBlockMatcher;
import org.xwiki.rendering.internal.block.BlockMatcherConverter;

import org.xwiki.properties.converter.ConversionException;

@RunWith(JUnitParamsRunner.class)
public class TestBlockMatcherConverter
{
    @Rule
    public ExpectedException thrown = ExpectedException.none ();
    
    @SuppressWarnings("unused")
    private Object[] correctBlocks () 
    {
        return new Object[]{
            new Object[] { "class:org.xwiki.rendering.block.WordBlock", WordBlock.class },
            new Object[] { "class:org.xwiki.rendering.block.TableBlock", TableBlock.class },
            new Object[] { "class:org.xwiki.rendering.block.RawBlock", RawBlock.class },
            new Object[] { "class:org.xwiki.rendering.block.LinkBlock", LinkBlock.class },
            new Object[] { "class:org.xwiki.rendering.block.ImageBlock", ImageBlock.class }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] incorrectBlocks () 
    {
        return new Object[]{
            new Object[] { "class:org.xwiki.rendering.WordBlock"},
            new Object[] { "ImageBlock" },
            new Object[] { "class:org.xwiki.rendering.ImageBlock" },
            new Object[] { "class;ImageBlock" },
            new Object[] { "org.xwiki.rendering.block.LinkBlock" }
        };
    }
    
    @Test
    @Parameters(method = "correctBlocks")
    public void testConvertToType (String block, Class<?> class_)
    {
        BlockMatcher matcher = new BlockMatcherConverter ().convert (BlockMatcher.class, block);
        
        assertTrue (matcher instanceof ClassBlockMatcher);
    }
    
    @Test
    @Parameters(method = "incorrectBlocks")
    public void testConvertToTypeException (String block)
    {
        thrown.expect (ConversionException.class);
        thrown.expectMessage (String.format("Unknown BlockMatcher [%s]", block.trim ()));
        new BlockMatcherConverter ().convert (BlockMatcher.class, block);
    }
}

