package xwiki.rendering.unit.test.api.internal.syntax;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import org.xwiki.rendering.parser.ParseException;
import org.xwiki.rendering.syntax.Syntax;
import org.xwiki.rendering.syntax.SyntaxFactory;
import org.xwiki.rendering.syntax.SyntaxType;

import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class TestDefaultSyntaxFactory
{
    @Rule
    public ExpectedException thrown = ExpectedException.none ();
    
    @SuppressWarnings("unused")
    private Object[] correctSyntax () 
    {
        return new Object[]{
            new Object[] { "correct/syntax_1", new Syntax (new SyntaxType ("correct", "type"), "syntax_1") },
            new Object[] { "/", new Syntax (new SyntaxType ("", ""), "") },
            new Object[] { " /", new Syntax (new SyntaxType (" ", "type"), "") },
            new Object[] { "/ ", new Syntax (new SyntaxType ("", "type"), " ") },
            new Object[] { " / ", new Syntax (new SyntaxType (" ", "type"), " ") }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] incorrectSyntax () 
    {
        return new Object[]{
            new Object[] { ""},
            new Object[] { "syntax" },
            new Object[] { "id-version" },
            new Object[] { "idversion" },
            new Object[] { "id\\version" }
        };
    }
    
    @Test
    @Parameters(method = "correctSyntax")
    public void testCreateSyntaxFromIdString (String syntax, Syntax obj_syntax) throws ParseException
    {
        SyntaxFactory sf = mock (SyntaxFactory.class);
        
        when (sf.createSyntaxFromIdString (syntax)).thenReturn (obj_syntax);
    }
    
    @Test
    @Parameters(method = "incorrectSyntax")
    public void testCreateSyntaxFromIdStringException (String syntax) throws ParseException
    {
        SyntaxFactory sf = mock (SyntaxFactory.class);
        
        when (sf.createSyntaxFromIdString (syntax)).thenThrow (new ParseException ("Invalid Syntax format [" + syntax + "]"));
    }
    
    @Test
    public void testCreateSyntaxFromIdStringExceptionWithNullSyntaxId () throws ParseException
    {
        SyntaxFactory sf = mock (SyntaxFactory.class);
        
        when (sf.createSyntaxFromIdString (null)).thenThrow (new ParseException("The passed Syntax cannot be NULL"));
    }
}
