package xwiki.rendering.unit.test.api.syntax;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.syntax.Syntax;
import org.xwiki.rendering.syntax.SyntaxType;

@RunWith(JUnitParamsRunner.class)
public class TestSyntax
{
    // We should separate methods with a flag (true or false) to test equals and not equals.
    // Anyway, in this way it seems more readable => every test method has the corresponding method for the parameters
    
    @SuppressWarnings("unused")
    private Object[] toIdStringValuesEquals () 
    {
        return new Object[]
        {
            new Object[] { new Syntax (SyntaxType.HTML, "VERSION"), "html/version" },
            new Object[] { new Syntax (SyntaxType.JSPWIKI, "1.1"), "jspwiki/1.1" },
            new Object[] { new Syntax (SyntaxType.DOCBOOK, "VeRsION-1.2"), "docbook/version-1.2" },
            new Object[] { new Syntax (SyntaxType.XHTML, "version1.1"), "xhtml/version1.1" },
            new Object[] { new Syntax (SyntaxType.MARKDOWN, "vers1.1"), "markdown/vers1.1" },
            new Object[] { new Syntax (SyntaxType.TEX, ""), "tex/" }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] toIdStringValuesNotEquals () 
    {
        return new Object[]
        {
            new Object[] { new Syntax (SyntaxType.HTML, "VERSION"), "html/VERSION" },
            new Object[] { new Syntax (SyntaxType.JSPWIKI, "1.1"), "Jspwiki/1.1" },
            new Object[] { new Syntax (SyntaxType.DOCBOOK, "VeRsION-1.2"), "docbook/VeRsION-1.2" },
            new Object[] { new Syntax (SyntaxType.XHTML, "version1.1"), "XHTML/version1.1" },
            new Object[] { new Syntax (SyntaxType.MARKDOWN, "vers1.1"), "Markdown/vers1.1" },
            new Object[] { new Syntax (SyntaxType.TEX, ""), "TeX/" }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] toStringValuesEquals () 
    {
        return new Object[]
        {
            new Object[] { new Syntax (SyntaxType.ANNOTATED_HTML, "VERSION", "quant1"), "Annotated HTML VERSION (quant1)" },
            new Object[] { new Syntax (SyntaxType.ANNOTATED_XHTML, "1.1", ""), "Annotated XHTML 1.1 ()" },
            new Object[] { new Syntax (SyntaxType.CONFLUENCE, "VeRsION-1.2", null), "Confluence VeRsION-1.2" },
            new Object[] { new Syntax (SyntaxType.CONFLUENCEXHTML, "version1.1", "Quantifier"), "Confluence version1.1 (Quantifier)" },
            new Object[] { new Syntax (SyntaxType.CREOLE, "vers1.1", "quant"), "Creole vers1.1 (quant)" },
            new Object[] { new Syntax (SyntaxType.PLAIN, "", null), "Plain " }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] toStringValuesNotEquals () 
    {
        return new Object[]
        {
            new Object[] { new Syntax (SyntaxType.ANNOTATED_HTML, "VERSION", "quant1"), "Annotated HTML VERSION/(quant1)" },
            new Object[] { new Syntax (SyntaxType.ANNOTATED_XHTML, "1.1", ""), "Annotated XHTML 1.1/()" },
            new Object[] { new Syntax (SyntaxType.CONFLUENCE, "VeRsION-1.2", null), "Confluence/VeRsION-1.2" },
            new Object[] { new Syntax (SyntaxType.CONFLUENCEXHTML, "version1.1", "Quantifier"), "Confluence/version1.1 (Quantifier)" },
            new Object[] { new Syntax (SyntaxType.CREOLE, "vers1.1", "quant"), "Creole vers1.1/(quant)" },
            new Object[] { new Syntax (SyntaxType.PLAIN, "", null), "Plain/" }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] _equals () 
    {
        return new Object[]
        {
            new Object[] 
            { 
                new Syntax (SyntaxType.ANNOTATED_HTML, "1.1", "quant1"), new Syntax (SyntaxType.ANNOTATED_HTML, "1.1", "quant1") 
            },
            new Object[] 
            { 
                new Syntax (SyntaxType.PLAIN, "1.1", "quant1"), new Syntax (SyntaxType.PLAIN, "1.1", "quant1") 
            },
            new Object[] 
            { 
                new Syntax (SyntaxType.CONFLUENCE, "1", "quant1"), new Syntax (SyntaxType.CONFLUENCE, "1", "quant1") 
            },
            new Object[] 
            { 
                new Syntax (null, null), new Syntax (null, null)
            }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] _notEquals () 
    {
        return new Object[]
        {
            new Object[] 
            { 
                new Syntax (SyntaxType.ANNOTATED_HTML, "1.1", "quant1"), new Syntax (SyntaxType.ANNOTATED_HTML, "1.0", "quant1") 
            },
            new Object[] 
            { 
                new Syntax (SyntaxType.PLAIN, "1.1", "quant1"), new Syntax (SyntaxType.PLAIN, "1.1", "quant") 
            },
            new Object[] 
            { 
                new Syntax (SyntaxType.CONFLUENCE, "1"), new Syntax (SyntaxType.CONFLUENCE, "1", "") 
            }
        };
    }
    
    @Test
    @Parameters(method = "toIdStringValuesEquals")
    public void testToIdStringEquals (Syntax syntax, String toIdString)
    {
        assertEquals (syntax.toIdString (), toIdString);
    }
    
    @Test
    @Parameters(method = "toIdStringValuesNotEquals")
    public void testToIdStringNotEquals (Syntax syntax, String toIdString)
    {
        assertFalse (syntax.toIdString ().equals (toIdString));
    }
    
    @Test
    @Parameters(method = "toStringValuesEquals")
    public void testToStringEquals (Syntax syntax, String toString)
    {
        assertEquals (syntax.toString (), toString);
    }

    @Test
    @Parameters(method = "toStringValuesNotEquals")
    public void testToStringNotEquals (Syntax syntax, String toString)
    {
        assertFalse (syntax.toIdString ().equals (toString));
    }

    @Test
    @Parameters(method = "_equals")
    public void testEquals (Syntax syntax_1, Syntax syntax_2)
    {
        assertEquals (syntax_1, syntax_2);
    }

    @Test
    @Parameters(method = "_notEquals")
    public void testNotEquals (Syntax syntax_1, Syntax syntax_2)
    {
        assertNotSame (syntax_1, syntax_2);
    }
}
