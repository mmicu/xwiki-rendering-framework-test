package xwiki.rendering.unit.test.api.syntax;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.syntax.SyntaxType;

@RunWith(JUnitParamsRunner.class)
public class TestSyntaxType
{
    // We should separate methods with a flag (true or false) to test equals and not equals.
    // Anyway, in this way it seems more readable => every test method has the corresponding method for the parameters
    
    @SuppressWarnings("unused")
    private Object[] instancesEquals () 
    {
        return new Object[]{
            new Object[] { new SyntaxType ("id 1", "name 1"), new SyntaxType ("id 1", "name 1") },
            new Object[] { new SyntaxType (null, null), new SyntaxType (null, null) },
            new Object[] { new SyntaxType ("", ""), new SyntaxType ("", "") }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] instancesSyntaxType () 
    {
        return new Object[]{
            new Object[] { "id 1", "name 1" },
            new Object[] { "syntax_id", "syntax_name" },
            new Object[] { "PHP", "PHP" },
            new Object[] { "", "" },
            new Object[] { null, null }
        };
    }
    
    @Test
    @Parameters(method = "instancesEquals")
    public void testEquals (SyntaxType s_1, SyntaxType s_2)
    {
        assertEquals (s_1, s_2);
    }
    
    @Test
    @Parameters(method = "instancesSyntaxType")
    public void testToIdStringEquals (String id, String name)
    {
        SyntaxType st = new SyntaxType (id, name);

        assertEquals (st.getId (), id);
        assertEquals (st.getName (), name);
    }
    
    @Test
    @Parameters(method = "instancesSyntaxType")
    public void testToString (String id, String name)
    {
        SyntaxType st = new SyntaxType (id, name);

        assertEquals (st.toString (), name);
    }
}
