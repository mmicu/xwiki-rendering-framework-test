package xwiki.rendering.unit.test.api.transformation;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Arrays;

import org.xwiki.rendering.block.Block;
import org.xwiki.rendering.block.IdBlock;
import org.xwiki.rendering.block.XDOM;
import org.xwiki.rendering.syntax.Syntax;
import org.xwiki.rendering.transformation.TransformationContext;

public class TestTransformationContext
{
    @Test
    public void testCloneEqualsContexts (TransformationContext t_1, TransformationContext t_2)
    {
        TransformationContext context_1 = new TransformationContext (),
                              context_2 = new TransformationContext (),
                              clone_1,
                              clone_2;
        
        XDOM xdom = new XDOM (Arrays.<Block>asList (new IdBlock ("test_block")));
        
        context_1.setId ("test_id_1"); context_1.setSyntax (Syntax.HTML_4_01);    context_1.setXDOM (xdom);
        context_2.setId ("test_id_2"); context_2.setSyntax (Syntax.MARKDOWN_1_0); context_2.setXDOM (xdom);
        
        clone_1 = context_1.clone ();
        clone_2 = context_2.clone ();
        
        // First instance
        assertNotSame (context_1, clone_1);
        assertEquals (context_1.getId (), clone_1.getId ());
        assertEquals (context_1.getSyntax (), clone_1.getSyntax ());
        assertEquals (context_1.getXDOM (), clone_1.getXDOM ());
        assertEquals (context_1.getTargetSyntax (), clone_1.getTargetSyntax ());
        
        // Second instance
        assertNotSame (context_2, clone_2);
        assertEquals (context_2.getId (), clone_2.getId ());
        assertEquals (context_2.getSyntax (), clone_2.getSyntax ());
        assertEquals (context_2.getXDOM (), clone_2.getXDOM ());
        assertEquals (context_2.getTargetSyntax (), clone_2.getTargetSyntax ());
    }
}
