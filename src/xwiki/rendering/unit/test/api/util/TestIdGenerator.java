package xwiki.rendering.unit.test.api.util;

import static org.junit.Assert.assertNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.util.IdGenerator;

@RunWith(JUnitParamsRunner.class)
public class TestIdGenerator
{
    @Rule
    public ExpectedException thrown = ExpectedException.none ();
    
    @SuppressWarnings("unused")
    private Object[] prefixesForGenerateUniqueIdMethod () 
    {
        return new Object[]{
            new Object[] { "", true }, // E_1 class
            new Object[] { "   ", true }, // E_1 class
            new Object[] { "onlyalphacharacters", false }, // E_2' class
            new Object[] { "simpleprefix", false }, // E_2' class
            new Object[] { "prefix  ,", true }, // E_2'' class
            new Object[] { "prefix{}", true } // E_2'' class
        };
    }
    
    @Test
    @Parameters(method = "prefixesForGenerateUniqueIdMethod")
    public void testGenerateUniqueId (String prefix, boolean exception)
    {
        if (exception) {
            thrown.expect (IllegalArgumentException.class);
            thrown.expectMessage (
                "The prefix [" + prefix + "] should only contain" +
                " alphanumerical characters and not be empty."
            );
            new IdGenerator ().generateUniqueId (prefix, "");
        }
        else {
            Exception ex = null;
            
            try {
                new IdGenerator ().generateUniqueId (prefix, "");
            }
            catch (IllegalArgumentException e) {
                ex = e;
            }
            
            assertNull (ex);
        }
    }
}
