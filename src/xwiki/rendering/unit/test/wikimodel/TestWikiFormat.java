package xwiki.rendering.unit.test.wikimodel;

import org.junit.Test;
import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.is;

import java.util.Arrays;
import java.util.HashSet;

import org.xwiki.rendering.wikimodel.WikiFormat;
import org.xwiki.rendering.wikimodel.WikiParameter;
import org.xwiki.rendering.wikimodel.WikiStyle;

public class TestWikiFormat
{
    @Test
    public void testStyles ()
    {
        WikiStyle ws_1 = new WikiStyle ("style 1"),
                  ws_2 = new WikiStyle ("style 2"),
                  ws_3 = new WikiStyle ("style 3"),
                  ws_4 = new WikiStyle ("style 4"),
                  ws_5 = new WikiStyle ("style 5");
        
        WikiFormat wf = new WikiFormat (new HashSet<WikiStyle> (Arrays.asList (ws_1, ws_2, ws_3, ws_4, ws_5)));
        
        assertThat (wf.getStyles ().size (), is (5));
        assertThat (wf.getParams ().size (), is (0));
        
        assertThat (wf.hasStyle (ws_1), is (true));
        assertThat (wf.hasStyle (ws_2), is (true));
        assertThat (wf.hasStyle (ws_3), is (true));
        assertThat (wf.hasStyle (ws_4), is (true));
        assertThat (wf.hasStyle (ws_5), is (true));
        
        assertThat (wf.hasStyle (new WikiStyle ("style 4 ")), is (false));
        assertThat (wf.hasStyle (new WikiStyle ("style 2 ")), is (false));
        assertThat (wf.hasStyle (new WikiStyle (" style 1")), is (false));
        
        assertThat (wf.removeStyle (new WikiStyle (" style 1")), is (wf));
        
        wf = wf.removeStyle (ws_1);
        assertThat (wf.getStyles ().size (), is (4));
        
        assertThat (wf.removeStyle (new WikiStyle (" style 1")), is (wf));
    }
    
    @Test
    public void testParameters ()
    {
        WikiParameter ws_1 = new WikiParameter ("key 1", "value 1"),
                      ws_2 = new WikiParameter ("key 2", "value 2"),
                      ws_3 = new WikiParameter ("key 3", "value 3"),
                      ws_4 = new WikiParameter ("key 4", "value 4"),
                      ws_5 = new WikiParameter ("key 5", "value 5");
        
        WikiFormat wf = new WikiFormat (Arrays.asList (ws_1, ws_2, ws_3, ws_4, ws_5));
        
        assertThat (wf.getParams ().size (), is (5));
    }
}
