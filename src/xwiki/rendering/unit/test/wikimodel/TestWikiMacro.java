package xwiki.rendering.unit.test.wikimodel;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.wikimodel.WikiMacro;
import org.xwiki.rendering.wikimodel.WikiParameters;

@RunWith(JUnitParamsRunner.class)
public class TestWikiMacro
{
    @SuppressWarnings("unused")
    private Object[] valuesForConstructor () 
    {
        return new Object[]
        {
            new Object[] { "name", new WikiParameters ("key", "value"), "content" },
            new Object[] { null, null, null },
            new Object[] { "", null, null },
            new Object[] { null, new WikiParameters (), null },
            new Object[] { null, null, " " },
            new Object[] { "", null, "" },
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] valuesForToString () 
    {
        return new Object[]
        {
            new Object[] 
            { 
                "name", 
                new WikiParameters ("key", "value"), 
                "content", 
                "{{name | content | " + new WikiParameters ("key", "value") + "}}" 
            },
            new Object[] 
            { 
                "name 2", 
                new WikiParameters ("key", "value"), 
                "content  2", 
                "{{name 2 | content  2 | " + new WikiParameters ("key", "value") + "}}" 
            },
        };
    }
    
    @Test
    @Parameters(method = "valuesForConstructor")
    public void testConstructor (String name, WikiParameters wp, String content)
    {
        WikiMacro wm = new WikiMacro (name, wp, content);
        
        assertEquals (wm.getName (), name);
        assertEquals (wm.getWikiParameters (), wp);
        assertEquals (wm.getContent (), content);
    }
    
    @Test
    @Parameters(method = "valuesForToString")
    public void testToString (String name, WikiParameters wp, String content, String toString)
    {
        WikiMacro wm = new WikiMacro (name, wp, content);
        
        assertEquals (wm.toString (), toString);
    }
}
