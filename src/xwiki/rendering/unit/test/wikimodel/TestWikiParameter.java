package xwiki.rendering.unit.test.wikimodel;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.wikimodel.WikiParameter;

@RunWith(JUnitParamsRunner.class)
public class TestWikiParameter
{
    @SuppressWarnings("unused")
    private Object[] valuesForConstructor () 
    {
        return new Object[]
        {
            new Object[] { "key 1", "value 1" },
            new Object[] { null, null },
            new Object[] { "  key  2  ", "  value   2  " },
            new Object[] { "", "" },
            new Object[] { "  ", null },
            new Object[] { null, "  " },
        };
    }
    
    @Test
    @Parameters(method = "valuesForConstructor")
    public void testConstructor (String key, String value)
    {
        WikiParameter wp = new WikiParameter (key, value);
        
        assertEquals (wp.getKey (), key);
        assertEquals (wp.getValue (), value);
    }
}
