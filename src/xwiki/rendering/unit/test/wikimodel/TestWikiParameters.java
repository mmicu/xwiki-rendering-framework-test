package xwiki.rendering.unit.test.wikimodel;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.xwiki.rendering.wikimodel.WikiParameter;
import org.xwiki.rendering.wikimodel.WikiParameters;

public class TestWikiParameters
{
    private WikiParameters wpInstance;
    
    @Before
    public void setUp ()
    {
        this.wpInstance = new WikiParameters ();
    }
    
    @Test
    public void testNewWikiParameters ()
    {
        assertEquals (WikiParameters.newWikiParameters ("    "), WikiParameters.newWikiParameters (""));
        assertEquals (WikiParameters.newWikiParameters ("    "), WikiParameters.newWikiParameters ("  "));
        assertEquals (WikiParameters.newWikiParameters (""), WikiParameters.newWikiParameters ("  "));
    }
    
    @Test
    public void testaddParameter ()
    {
        assertTrue (this.wpInstance.getSize () == 0);
        this.wpInstance.addParameter ("key 1", "value 1");
        this.wpInstance.addParameter ("key 2", "value 2");
        this.wpInstance.addParameter ("key 3", "value 3");
        assertTrue ("this.wpInstance.getSize () = " + this.wpInstance.getSize (), this.wpInstance.getSize () == 3);
        assertTrue ("this.wpInstance.toList () = " + this.wpInstance.toList ().size (), this.wpInstance.toList ().size () == 3);
        
        this.wpInstance.remove ("unknown key");
        assertTrue (this.wpInstance.getSize () == 3);
        
        this.wpInstance.remove ("key 1 ");
        assertTrue (this.wpInstance.getSize () == 3);
        
        this.wpInstance.remove ("key 1");
        assertTrue (this.wpInstance.getSize () == 2);
        
        assertEquals (this.wpInstance.getParameter (3), null);
        assertEquals (this.wpInstance.getParameter (2), new WikiParameter ("key 3", "value 3"));
    }
}
