package xwiki.rendering.unit.test.wikimodel;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.xwiki.rendering.wikimodel.WikiParameters;
import org.xwiki.rendering.wikimodel.WikiReference;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class TestWikiReference
{
    @SuppressWarnings("unused")
    private Object[] constructorOneParameter () 
    {
        return new Object[]
        {
            new Object[] { new WikiReference ("link 1"), "link 1" },
            new Object[] { new WikiReference (""), "" },
            new Object[] { new WikiReference ("  link  "), "  link  " },
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] constructorTwoParameters () 
    {
        return new Object[]
        {
            new Object[] { new WikiReference ("", "label "), "label " },
            new Object[] { new WikiReference ("", ""), "" },
            new Object[] { new WikiReference ("  link  ", "  label  "), "  label  " },
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] constructorThreeParameters () 
    {
        return new Object[]
        {
            new Object[] { new WikiReference ("", "label ", null), new WikiParameters () },
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] valuesForEquals () 
    {
        return new Object[]
        {
            new Object[] { new WikiReference (" ", "label 1", null), new WikiReference (" ", "label 1", null) },
            new Object[] { new WikiReference (" link  ", "label     2", null), new WikiReference (" link  ", "label     2", null) }
        };
    }
    
    @SuppressWarnings("unused")
    private Object[] valuesForToString () 
    {
        return new Object[]
        {
            new Object[] { new WikiReference (" ", "label 1", null), " (label 1)" },
            new Object[] { new WikiReference (" ", "label   2"), " (label   2)" },
            new Object[] { new WikiReference ("   ", "label 3  "), "   (label 3  )" }
        };
    }
    
    @Test
    @Parameters(method = "constructorOneParameter")
    public void testConstructorOneParameter (WikiReference wr, String link)
    {
        assertEquals (wr.getLink (), link);
    }
    
    @Test
    @Parameters(method = "constructorTwoParameters")
    public void testConstructorTwoParameters (WikiReference wr, String label)
    {
        assertEquals (wr.getLabel (), label);
    }
    
    @Test
    @Parameters(method = "constructorThreeParameters")
    public void testConstructorThreeParameters (WikiReference wr, WikiParameters params)
    {
        assertEquals (wr.getParameters (), params);
    }
    
    @Test
    @Parameters(method = "valuesForEquals")
    public void testEquals (WikiReference wr_1, WikiReference wr_2)
    {
        assertEquals (wr_1, wr_2);
    }
    
    @Test
    @Parameters(method = "valuesForToString")
    public void testToString (WikiReference wr, String toString)
    {
        assertEquals (wr.toString (), toString);
    }
}
