package xwiki.rendering.unit.test.wikimodel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.xwiki.rendering.wikimodel.WikiReference;
import org.xwiki.rendering.wikimodel.WikiReferenceParser;

@RunWith(JUnitParamsRunner.class)
public class TestWikiReferenceParser
{
    
    @Mock private WikiReferenceParser mockWikiRP;
    
    @SuppressWarnings("unused")
    private Object[] valuesForParseMethod () 
    {
        return new Object[]
        {
            new Object[] { "link 1[|>]label 2", new WikiReference ("link 1", "label 1") },
            new Object[] { "  link 2 [|>]  label 2  ", new WikiReference ("  link 2 ", "  label 2  ") },
            new Object[] { "no delimiter", null },
            new Object[] { "[|>]", new WikiReference ("", "") }
        };
    }
    
    @Before
    public void setUp ()
    {
        this.mockWikiRP = mock (WikiReferenceParser.class);
    }
    
    @Test
    @Parameters(method = "valuesForParseMethod")
    public void testParse (String s, WikiReference wp)
    {
        when (mockWikiRP.parse (s)).thenReturn (wp);
    }
}
