package xwiki.rendering.unit.test.wikimodel;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.xwiki.rendering.wikimodel.WikiStyle;

@RunWith(JUnitParamsRunner.class)
public class TestWikiStyle
{
    @SuppressWarnings("unused")
    private Object[] wikimodelInstances () 
    {
        return new Object[]
        {
            new Object[] { "Wikimodel" },
            new Object[] { "" },
            new Object[] { "  " },
            new Object[] { "Wikimodel  " }
        };
    }
    
    @Test
    @Parameters(method = "wikimodelInstances")
    public void testToString (String fName)
    {
        assertEquals (new WikiStyle (fName).toString (), fName.toString ());
    }
    
    @Test
    @Parameters(method = "wikimodelInstances")
    public void testHashCode (String fName)
    {
        assertEquals (new WikiStyle (fName).hashCode (), fName.hashCode ());
    }
}
